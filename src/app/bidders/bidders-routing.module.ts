import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BiddersPage } from './bidders.page';

const routes: Routes = [
  {
    path: '',
    component: BiddersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BiddersPageRoutingModule {}
