import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BiddersPageRoutingModule } from './bidders-routing.module';

import { BiddersPage } from './bidders.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BiddersPageRoutingModule
  ],
  declarations: [BiddersPage]
})
export class BiddersPageModule {}
