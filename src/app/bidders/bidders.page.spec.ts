import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BiddersPage } from './bidders.page';

describe('BiddersPage', () => {
  let component: BiddersPage;
  let fixture: ComponentFixture<BiddersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BiddersPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BiddersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
