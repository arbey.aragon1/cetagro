import { Component, OnInit, ViewChild } from "@angular/core";
import { IonInfiniteScroll, NavController } from "@ionic/angular";
import { BehaviorSubject } from "rxjs";
import { switchMap, map } from "rxjs/operators";

interface FilterData {
  filterName: string;
}

@Component({
  selector: "app-bidders",
  templateUrl: "./bidders.page.html",
  styleUrls: ["./bidders.page.scss"]
})
export class BiddersPage {
  @ViewChild(IonInfiniteScroll, { static: false })
  infiniteScroll: IonInfiniteScroll;

  data: any[] = [];
  private dataObs: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([
    "a1",
    "a2",
    "a3",
    "a4",
    "a5",
    "a1",
    "a2",
    "a3",
    "a4",
    "a5",
    "a1",
    "a2",
    "a3",
    "a4",
    "a5",
    "a1",
    "a2",
    "a3",
    "a4",
    "a5"
  ]);

  private dataFilter: BehaviorSubject<FilterData> = new BehaviorSubject<
    FilterData
  >({ filterName: "" });

  constructor(private navCtrl: NavController) {}

  ionViewWillEnter() {
    this.dataObs
      .pipe(
        switchMap((data: any[]) => {
          return this.dataFilter.pipe(
            map((df: FilterData) => {
              return data.filter(
                word =>
                  word.toLocaleLowerCase().search(df.filterName + "") >= 0 ||
                  df.filterName.length == 0
              );
            })
          );
        })
      )
      .subscribe((data: any[]) => {
        this.data = data;
      });
  }

  loadData(event) {
    setTimeout(() => {
      this.dataObs.next([...this.data, ...["a1", "b2", "c3", "d4", "e5"]]);

      event.target.complete();

      if (this.dataObs.value.length === 500) {
        this.infiniteScroll.disabled = true;
      }
    }, 500);
  }

  onSearchChange($event) {
    this.dataFilter.next({ filterName: $event.detail.value });
  }

  goToPage(dir: string) {
    this.navCtrl.navigateForward(dir);
  }
}
