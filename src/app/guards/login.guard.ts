import { Injectable } from "@angular/core";
import {
  CanActivate,
  CanActivateChild,
  CanLoad,
  Route,
  UrlSegment,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree
} from "@angular/router";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class LoginGuard implements CanActivate {
  constructor(private router: Router, private storage: Storage) {}

  async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    /*const isIntroShowed = await this.storage.get("isIntroShowed");

    if (isIntroShowed) {
      return true;
    } else {
      this.router.navigateByUrl("/intro");
    }/** */
    return true;
  }
}
