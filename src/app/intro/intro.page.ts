import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { SwUpdate } from "@angular/service-worker";
import { Component, HostListener, OnInit } from "@angular/core";

@Component({
  selector: "app-intro",
  templateUrl: "./intro.page.html",
  styleUrls: ["./intro.page.scss"]
})
export class IntroPage implements OnInit {
  slideOpt = {
    initialSlide: 0,
    slidePerView: 1,
    centeredSlides: true,
    speed: 400
  };

  slides = [
    {
      title: "Este es el titulo",
      subTitle: "Subtitulo",
      description: "descripcion",
      icon: "play"
    },
    {
      title: "Este es el titulo",
      subTitle: "Subtitulo",
      description: "descripcion",
      icon: "play"
    },
    {
      title: "Este es el titulo",
      subTitle: "Subtitulo",
      description: "descripcion",
      icon: "play"
    }
  ];

  installEvent = null;
  constructor(
    private router: Router,
    private storage: Storage,
    private swUpdate: SwUpdate
  ) {}

  ngOnInit() {
    this.updatePWA();
  }

  finish() {
    this.storage.set("isIntroShowed", true);
    this.router.navigateByUrl("/login");
  }

  updatePWA() {
    this.swUpdate.available.subscribe(value => {
      console.log("update", value);
      window.location.reload();
    });
  }

  @HostListener("window:beforeinstallprompt", ["$event"])
  onBeforeInstallPrompt(event: Event) {
    console.log(event);
    event.preventDefault();
    this.installEvent = event;
  }

  installByUser() {
    if (this.installEvent) {
      this.installEvent.prompt();
      this.installEvent.userChoice().then(rta => {
        console.log(rta);
      });
    }
  }
}
