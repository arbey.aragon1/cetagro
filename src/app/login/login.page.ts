import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { NavController } from "@ionic/angular";
import { AuthenticateService } from "../services/authenticate.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;

  validation_message = {
    email: [
      { type: "required", message: "El email es requerido" },
      { type: "pattern", message: "Este no es un email valido" },
    ],
    password: [
      { type: "required", message: "El password es requerido" },
      { type: "minlength", message: "Minimo 6 letras para el password" },
    ],
  };

  errorMessage = "";

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthenticateService,
    private navCtrl: NavController
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"),
        ])
      ),
      password: new FormControl(
        "",
        Validators.compose([Validators.required, Validators.minLength(6)])
      ),
    });
  }

  loginUser(credentials) {
    this.authService
      .signInWithEmailAndPassword(credentials.email, credentials.password)
      .subscribe(() => {
        this.navCtrl.navigateForward("/menu/home");
      });
  }

  logout() {
    this.authService.logout().subscribe(() => {});
  }

  authGoogle() {
    this.authService.signInWithGoogle().subscribe(() => {
      this.navCtrl.navigateForward("/menu/home");
    });
  }

  sendPasswordResetEmail(email: string) {
    this.authService.sendPasswordResetEmail(email).subscribe();
  }

  goToRegister() {
    this.navCtrl.navigateForward("/signup");
  }

  goToPasswordReset() {
    this.navCtrl.navigateForward("/password-reset");
  }
}
