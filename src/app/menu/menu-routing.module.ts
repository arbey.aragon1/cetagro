import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { MenuPage } from "./menu.page";

const routes: Routes = [
  {
    path: "",
    component: MenuPage,
    children: [
      {
        path: "home",
        loadChildren: () =>
          import("../home/home.module").then((m) => m.HomePageModule),
      },
      {
        path: "settings",
        loadChildren: () =>
          import("../settings/settings.module").then(
            (m) => m.SettingsPageModule
          ),
      },
      {
        path: "public-offers",
        loadChildren: () =>
          import("../public-offers/public-offers.module").then(
            (m) => m.PublicOffersPageModule
          ),
      },
      {
        path: "my-offers",
        loadChildren: () =>
          import("../my-offers/my-offers.module").then(
            (m) => m.MyOffersPageModule
          ),
      },
      {
        path: "bidders",
        loadChildren: () =>
          import("../bidders/bidders.module").then((m) => m.BiddersPageModule),
      },

      {
        path: "user-profile",
        loadChildren: () =>
          import("../user-profile/user-profile.module").then(
            (m) => m.UserProfilePageModule
          ),
      },
      {
        path: "user-profile-editor",
        loadChildren: () =>
          import("../user-profile-editor/user-profile-editor.module").then(
            (m) => m.UserProfileEditorPageModule
          ),
      },
      {
        path: "chat",
        loadChildren: () =>
          import("../chat/chat.module").then((m) => m.ChatPageModule),
      },
      {
        path: "user-detail",
        loadChildren: () =>
          import("../user-detail/user-detail.module").then(
            (m) => m.UserDetailPageModule
          ),
      },
      {
        path: "offer-detail",
        loadChildren: () =>
          import("../offer-detail/offer-detail.module").then(
            (m) => m.OfferDetailPageModule
          ),
      },
      {
        path: "offer-editor",
        loadChildren: () =>
          import("../offer-editor/offer-editor.module").then(
            (m) => m.OfferEditorPageModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
