import { Component, OnInit } from "@angular/core";
import { MenuController, NavController } from "@ionic/angular";

enum IndexMenu {
  Home,
  PublicOffers,
  MyOffers,
  Bidders,
  UserProfile,
  Settings,
  Login,
}

type MenuData = {
  [key in IndexMenu]: {
    path: string;
  };
};

@Component({
  selector: "app-menu",
  templateUrl: "./menu.page.html",
  styleUrls: ["./menu.page.scss"],
})
export class MenuPage implements OnInit {
  indexMenu = IndexMenu;
  currentIndex: IndexMenu = IndexMenu.Home;
  indexData: MenuData = {
    [IndexMenu.Home]: {
      path: "/menu/home",
    },
    [IndexMenu.PublicOffers]: {
      path: "/menu/public-offers",
    },
    [IndexMenu.MyOffers]: {
      path: "/menu/my-offers",
    },
    [IndexMenu.Bidders]: {
      path: "/menu/bidders",
    },
    [IndexMenu.UserProfile]: {
      path: "/menu/user-profile",
    },
    [IndexMenu.Settings]: {
      path: "/menu/settings",
    },
    [IndexMenu.Login]: {
      path: "/login",
    },
  };

  constructor(private menu: MenuController, private navCtrl: NavController) {}

  ngOnInit() {
    this.currentIndex = IndexMenu.Home;
  }

  closeMenu() {
    this.menu.close();
  }

  goToPage(index: IndexMenu) {
    this.currentIndex = index;
    this.menu.close();
    this.navCtrl.navigateForward(this.indexData[index].path);
  }
}
