export interface ImagesObj {
  [key: string]: string;
}

export interface OfferInterface {
  key?: string;
  status?: number;
  publicOffer?: boolean;
  bidderUID?: string;
  quantity?: string;
  imageURLs?: ImagesObj;
  title?: string;
  description?: string;
  labels?: string[];
}

export class Offer {
  constructor(
    private _key: string,
    private _status: number,
    private _publicOffer: boolean,
    private _bidderUID: string,
    private _quantity: string,
    private _imageURLs: ImagesObj,
    private _title: string,
    private _description: string,
    private _labels: string[]
  ) {}

  get key(): string {
    return this._key;
  }

  get status(): number {
    return this._status;
  }

  get publicOffer(): boolean {
    return this._publicOffer;
  }

  get bidderUID(): string {
    return this._bidderUID;
  }

  get quantity(): string {
    return this._quantity;
  }

  get imageURLs(): ImagesObj {
    return this._imageURLs;
  }

  get title(): string {
    return this._title;
  }

  get description(): string {
    return this._description;
  }

  get labels(): string[] {
    return this._labels;
  }

  public static getEmpty(): Offer {
    return new Offer(null, null, false, "", "", {}, "", "", []);
  }

  public static fromJSON({
    key,
    status,
    publicOffer,
    bidderUID,
    quantity,
    imageURLs,
    title,
    description,
    labels,
  }: OfferInterface): Offer {
    return new Offer(
      key,
      status,
      publicOffer,
      bidderUID,
      quantity,
      imageURLs,
      title,
      description,
      labels
    );
  }

  public static fromJSONArray(array: any[]): Offer[] {
    return array
      .map((value: any) => {
        return { key: value.key, ...value.payload.val() };
      })
      .map(Offer.fromJSON);
  }

  /**
   * README:
   * toJSON no utiliza el key del usuario
   * Se debe utilizar este método para guardar el usuario en
   * firebase con el método push
   */
  public toJSON(): OfferInterface {
    return {
      status: this.status,
      publicOffer: this.publicOffer,
      bidderUID: this.bidderUID,
      quantity: this.quantity,
      imageURLs: this.imageURLs,
      title: this.title,
      description: this.description,
      labels: this.labels,
    };
  }
}
