import { Component, ViewChild } from "@angular/core";
import { IonInfiniteScroll, NavController } from "@ionic/angular";
import { BehaviorSubject } from "rxjs";
import { switchMap, map, take } from "rxjs/operators";
import { OfferService } from "../services/offer.service";
import { AuthenticateService } from "../services/authenticate.service";
import { Offer } from "../models/offer";
import { OfferStatus } from "src/utils/constants";
import { NavigationExtras } from "@angular/router";

interface FilterData {
  filterName: string;
}

@Component({
  selector: "app-my-offers",
  templateUrl: "./my-offers.page.html",
  styleUrls: ["./my-offers.page.scss"],
})
export class MyOffersPage {
  @ViewChild(IonInfiniteScroll, { static: false })
  infiniteScroll: IonInfiniteScroll;

  data: Offer[] = [];
  private dataObs: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  private dataFilter: BehaviorSubject<FilterData> = new BehaviorSubject<
    FilterData
  >({ filterName: "" });

  constructor(
    private navCtrl: NavController,
    private offerService: OfferService,
    private authService: AuthenticateService
  ) {}

  ionViewWillEnter() {
    this.offerService
      .fetchMyOffers(this.authService.user.uid)
      .pipe(
        switchMap((data: Offer[]) => {
          return this.dataFilter.pipe(
            map((filter: FilterData) => {
              return data.filter(
                (o: Offer) =>
                  o.title.toLocaleLowerCase().search(filter.filterName + "") >=
                    0 || filter.filterName.length === 0
              );
            })
          );
        })
      )
      .subscribe((data: Offer[]) => {
        this.data = data;
      });
  }

  loadData(event) {
    /*setTimeout(() => {
      this.dataObs.next([...this.data, ...["a1", "b2", "c3", "d4", "e5"]]);

      event.target.complete();

      if (this.dataObs.value.length === 500) {
        this.infiniteScroll.disabled = true;
      }
    }, 500);/** */
  }

  onSearchChange($event) {
    this.dataFilter.next({ filterName: $event.detail.value });
  }

  goToPage(dir: string) {
    this.navCtrl.navigateForward(dir);
  }

  editOffer(offer: Offer): void {
    const data: { currentStatus: OfferStatus; key: string } = {
      currentStatus: offer.status,
      key: offer.key,
    };
    const navigationExtras: NavigationExtras = {
      queryParams: {
        data,
      },
    };
    this.navCtrl.navigateForward("/menu/offer-editor", navigationExtras);
  }

  addOffer() {
    const data: { currentStatus: OfferStatus; key: string } = {
      currentStatus: undefined,
      key: "",
    };
    const navigationExtras: NavigationExtras = {
      queryParams: {
        data,
      },
    };
    this.navCtrl.navigateForward("/menu/offer-editor", navigationExtras);
  }
}
