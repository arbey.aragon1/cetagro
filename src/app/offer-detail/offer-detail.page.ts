import { Component, OnInit } from "@angular/core";
import { NavController } from "@ionic/angular";
import { FileService } from "../services/file.service";
import { finalize } from "rxjs/operators";
import { Observable, from } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import { OfferStatus } from "src/utils/constants";
import { Offer } from "../models/offer";

@Component({
  selector: "app-offer-detail",
  templateUrl: "./offer-detail.page.html",
  styleUrls: ["./offer-detail.page.scss"],
})
export class OfferDetailPage implements OnInit {
  images = [
    "linkedin_profile_image.png",
    "linkedin_profile_image.png",
    "linkedin_profile_image.png",
    "linkedin_profile_image.png",
  ];

  sliderConfig = {
    slidesPerView: 1.6,
    spaceBetween: 10,
    centeredSlides: true,
  };
  offer: Offer;

  constructor(private navCtrl: NavController, private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      this.offer = params["data"];
    });
  }

  goToBack() {
    this.navCtrl.back();
  }
}
