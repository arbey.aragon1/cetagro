import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OfferEditorPage } from './offer-editor.page';

const routes: Routes = [
  {
    path: '',
    component: OfferEditorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OfferEditorPageRoutingModule {}
