import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { OfferEditorPageRoutingModule } from "./offer-editor-routing.module";

import { OfferEditorPage } from "./offer-editor.page";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OfferEditorPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [OfferEditorPage],
})
export class OfferEditorPageModule {}
