import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OfferEditorPage } from './offer-editor.page';

describe('OfferEditorPage', () => {
  let component: OfferEditorPage;
  let fixture: ComponentFixture<OfferEditorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfferEditorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OfferEditorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
