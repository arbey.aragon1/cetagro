import { Component, OnInit } from "@angular/core";
import { NavController, PickerController } from "@ionic/angular";
import { FileService } from "../services/file.service";
import { take, tap, switchMap, map } from "rxjs/operators";
import { Observable, of } from "rxjs";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { fromEvent } from "rxjs";
import { pluck } from "rxjs/operators";
import { OfferService } from "../services/offer.service";
import { AuthenticateService } from "../services/authenticate.service";
import { Offer, OfferInterface } from "../models/offer";
import { OfferStatus } from "src/utils/constants";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-offer-editor",
  templateUrl: "./offer-editor.page.html",
  styleUrls: ["./offer-editor.page.scss"],
})
export class OfferEditorPage implements OnInit {
  imageURL: string;
  downloadURL: Observable<string>;
  uploadPercent: number;

  listImages: any[] = [];
  categoryList: string[] = [];
  defaultColumnOptions: any = [
    {
      name: "Clases",
      options: [
        {
          text: "Papas",
          value: 0,
        },
        {
          text: "Frutas",
          value: 1,
        },
        {
          text: "hortalizas",
          value: 2,
        },
      ],
    },
  ];
  chips = new Set();
  chipList = [];
  dataFromMyOffers: { currentStatus: OfferStatus; key: string };
  public offerOld: Offer;

  registerForm: FormGroup;
  validationMessage = {
    title: [{ type: "required", message: "El titulo es requerido" }],
    quantity: [{ type: "required", message: "El titulo es requerido" }],
    description: [{ type: "required", message: "El titulo es requerido" }],
  };
  errorMessage = "";

  constructor(
    private formBuilder: FormBuilder,
    private navCtrl: NavController,
    private fileService: FileService,
    private pickerCtrl: PickerController,
    private authService: AuthenticateService,
    private offerService: OfferService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      this.dataFromMyOffers = params["data"];
    });

    this.createForm(Offer.getEmpty());
    if (this.dataFromMyOffers.key !== "") {
      this.offerService
        .fetchOffer(this.authService.user.uid, this.dataFromMyOffers.key)
        .pipe(
          take(1),
          tap((offer: Offer) => {
            this.offerOld = offer;
            console.log(offer);
            this.registerForm.setValue({
              title: this.offerOld.title,
              quantity: this.offerOld.quantity,
              publicOffer: this.offerOld.publicOffer,
              description: this.offerOld.description,
            });
            this.chipList = this.offerOld.labels;
            this.chips = new Set(this.chipList);
          })
        )
        .subscribe();
    }
  }

  createForm(offer: Offer) {
    console.log(offer);
    this.chipList = offer.labels;
    this.chips = new Set(this.chipList);
    this.registerForm = this.formBuilder.group({
      title: new FormControl(
        offer.title,
        Validators.compose([Validators.required])
      ),
      quantity: new FormControl(
        offer.quantity,
        Validators.compose([Validators.required])
      ),
      publicOffer: new FormControl(offer.publicOffer),
      description: new FormControl(
        offer.description,
        Validators.compose([Validators.required])
      ),
    });
  }

  removeImage(index: number) {
    this.listImages = this.listImages.filter((_, i) => {
      return index !== i;
    });
  }

  imageToBase64(fileReader: FileReader, fileToRead: File): Observable<string> {
    fileReader.readAsDataURL(fileToRead);
    return fromEvent(fileReader, "load").pipe(pluck("currentTarget", "result"));
  }

  fileChanges(event) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const fileReader = new FileReader();
      let imageToUpload = event.target.files.item(0);
      this.imageToBase64(fileReader, imageToUpload).subscribe((base64image) => {
        this.listImages.push({
          name: file.name,
          base64image: base64image,
          file: file,
        });
      });
    }
  }

  async addChip() {
    const picker = await this.pickerCtrl.create({
      columns: this.defaultColumnOptions,
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
        },
        {
          text: "Confirm",
          handler: (value) => {
            this.chips.add(value.Clases.text);
            this.chipList = [...this.chips];
          },
        },
      ],
    });

    await picker.present();
  }

  deleteChip(chip) {
    this.chips.delete(chip);
    this.chipList = [...this.chips];
  }

  uploadFile() {
    /*if (this.eventFile !== undefined) {
      const { task, percentage, fileRef } = this.fileService.uploadFile(
        this.eventFile,
        "uit-test",
        "file-name"
      );
      task
        .pipe(finalize(() => (this.downloadURL = fileRef.getDownloadURL())))
        .subscribe();

      percentage.subscribe((value: number) => (this.uploadPercent = value));
    }/** */
  }

  createOffer(): Observable<any> {
    if (
      this.dataFromMyOffers &&
      this.dataFromMyOffers.currentStatus === undefined
    ) {
      console.log("Crea usuario vacio");
      return this.offerService
        .addOffer(this.authService.user.uid, Offer.getEmpty())
        .pipe(
          take(1),
          tap((offer: Offer) => {
            this.offerOld = offer;
          }),
          map(() => undefined)
        );
    } else {
      console.log("No Crea usuario vacio");
      return of(undefined);
    }
  }

  updateOffer(credentials) {
    console.log(credentials);

    this.createOffer()
      .pipe(
        switchMap(() => {
          const offerNew: OfferInterface = {
            status: credentials.publicOffer
              ? OfferStatus.publicOffer
              : OfferStatus.onlyForUser,
            publicOffer: credentials.publicOffer,
            bidderUID: this.offerOld.bidderUID,
            quantity: credentials.quantity,
            imageURLs: {},
            title: credentials.title,
            description: credentials.description,
            labels: this.chipList,
          };

          console.log("Actualiza oferta");
          console.log(offerNew);
          console.log(
            this.authService.user.uid,
            this.offerOld.key,
            this.offerOld.status,
            credentials.publicOffer
              ? OfferStatus.publicOffer
              : OfferStatus.onlyForUser,
            offerNew
          );

          return this.offerService.updateOffer(
            this.authService.user.uid,
            this.offerOld.key,
            this.offerOld.status,
            credentials.publicOffer
              ? OfferStatus.publicOffer
              : OfferStatus.onlyForUser,
            offerNew
          );
        }, take(1))
      )
      .subscribe(
        () => {
          console.log("test");
          this.goToBack();
        },
        (err) => {
          this.goToBack();
          console.log(err);
        }
      );
  }

  goToBack() {
    this.navCtrl.back();
  }
}
