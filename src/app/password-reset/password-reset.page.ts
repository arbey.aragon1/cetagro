import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { NavController } from "@ionic/angular";
import { AuthenticateService } from "../services/authenticate.service";

@Component({
  selector: "app-password-reset",
  templateUrl: "./password-reset.page.html",
  styleUrls: ["./password-reset.page.scss"],
})
export class PasswordResetPage implements OnInit {
  loginForm: FormGroup;

  validation_message = {
    email: [
      { type: "required", message: "El email es requerido" },
      { type: "pattern", message: "Este no es un email valido" },
    ],
  };

  errorMessage = "";

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthenticateService,
    private navCtrl: NavController
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"),
        ])
      ),
    });
  }

  passwordReset(credentials) {
    console.log(credentials);
    this.authService
      .sendPasswordResetEmail(credentials.email)
      .subscribe(() => this.goToBack());
  }

  goToBack() {
    this.navCtrl.back();
  }
}
