import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PublicOffersPage } from './public-offers.page';

const routes: Routes = [
  {
    path: '',
    component: PublicOffersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicOffersPageRoutingModule {}
