import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PublicOffersPageRoutingModule } from './public-offers-routing.module';

import { PublicOffersPage } from './public-offers.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PublicOffersPageRoutingModule
  ],
  declarations: [PublicOffersPage]
})
export class PublicOffersPageModule {}
