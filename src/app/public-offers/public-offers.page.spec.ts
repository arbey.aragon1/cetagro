import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PublicOffersPage } from './public-offers.page';

describe('PublicOffersPage', () => {
  let component: PublicOffersPage;
  let fixture: ComponentFixture<PublicOffersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicOffersPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PublicOffersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
