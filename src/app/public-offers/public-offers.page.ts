import { Component, OnInit, ViewChild } from "@angular/core";
import { IonInfiniteScroll, NavController } from "@ionic/angular";
import { BehaviorSubject } from "rxjs";
import { switchMap, map } from "rxjs/operators";
import { Offer } from "../models/offer";
import { OfferService } from "../services/offer.service";
import { NavigationExtras } from "@angular/router";
import { OfferStatus } from "src/utils/constants";

interface FilterData {
  filterName: string;
}

@Component({
  selector: "app-public-offers",
  templateUrl: "./public-offers.page.html",
  styleUrls: ["./public-offers.page.scss"],
})
export class PublicOffersPage {
  @ViewChild(IonInfiniteScroll, { static: false })
  infiniteScroll: IonInfiniteScroll;

  data: Offer[] = [];
  private dataObs: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([
    "a1",
    "a2",
    "a3",
    "a4",
    "a5",
    "a1",
    "a2",
    "a3",
    "a4",
    "a5",
    "a1",
    "a2",
    "a3",
    "a4",
    "a5",
    "a1",
    "a2",
    "a3",
    "a4",
    "a5",
  ]);

  private dataFilter: BehaviorSubject<FilterData> = new BehaviorSubject<
    FilterData
  >({ filterName: "" });

  constructor(
    private navCtrl: NavController,
    private offerService: OfferService
  ) {}

  ionViewWillEnter() {
    this.offerService
      .fetchPublicOffers()
      .pipe(
        switchMap((data: Offer[]) => {
          return this.dataFilter.pipe(
            map((df: FilterData) => {
              return data.filter(
                (o: Offer) =>
                  (o.title &&
                    o.title.toLocaleLowerCase().search(df.filterName + "") >=
                      0) ||
                  df.filterName.length === 0
              );
            })
          );
        })
      )
      .subscribe((data: Offer[]) => {
        this.data = data;
      });
  }

  loadData(event) {
    setTimeout(() => {
      this.dataObs.next([...this.data, ...["a1", "b2", "c3", "d4", "e5"]]);

      event.target.complete();

      if (this.dataObs.value.length === 500) {
        this.infiniteScroll.disabled = true;
      }
    }, 500);
  }

  onSearchChange($event) {
    this.dataFilter.next({ filterName: $event.detail.value });
  }

  goToPage(offer: Offer) {
    const data: Offer = offer;
    const navigationExtras: NavigationExtras = {
      queryParams: {
        data,
      },
    };
    this.navCtrl.navigateForward("/menu/offer-detail", navigationExtras);
  }
}
