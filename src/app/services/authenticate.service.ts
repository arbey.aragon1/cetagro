import { Injectable } from "@angular/core";
import { UserService } from "./user.service";
import { User } from "../models/user";
import { AngularFireAuth } from "angularfire2/auth";
import {
  Observable,
  from,
  BehaviorSubject,
  of,
  Subscription,
  throwError,
} from "rxjs";
import { map, tap, switchMap, combineLatest, catchError } from "rxjs/operators";
import * as firebase from "firebase/app";
import { NavController } from "@ionic/angular";

@Injectable({
  providedIn: "root",
})
export class AuthenticateService {
  private _isLoggedIn: boolean = false;
  private _user: BehaviorSubject<User> = new BehaviorSubject<User>(undefined);
  private _userServiceSub: Subscription;

  get isLoggedIn(): boolean {
    return this._isLoggedIn;
  }

  constructor(
    private firebaseAuth: AngularFireAuth,
    private userService: UserService,
    private navCtrl: NavController
  ) {}

  public signInWithGoogle(): Observable<any> {
    return from(
      this.firebaseAuth.auth.signInWithPopup(
        new firebase.auth.GoogleAuthProvider()
      )
    ).pipe(
      switchMap((data: firebase.auth.UserCredential) => {
        return this.userService.fetchEmail(data.user.email).pipe(
          switchMap((exist: boolean) => {
            const user: User = new User(
              null,
              0,
              data.user.displayName,
              null,
              data.user.email,
              null
            );
            if (exist) {
              return this.userService.fetchUserByUid(data.user.uid).pipe(
                switchMap((userData: User) => {
                  if (userData.status !== undefined) {
                    return of(userData);
                  }
                  return this.userService
                    .addUserForFirstTime(data.user.uid, data.user.email, user)
                    .pipe(map(() => user));
                })
              );
            } else {
              return this.userService
                .addUserForFirstTime(data.user.uid, data.user.email, user)
                .pipe(map(() => user));
            }
          }),
          tap(() => this.postLogin(data.user.uid))
        );
      })
    );
  }

  public signInWithEmailAndPassword(
    email: string,
    password: string
  ): Observable<any> {
    return from(
      this.firebaseAuth.auth.signInWithEmailAndPassword(email, password)
    ).pipe(
      tap((data: firebase.auth.UserCredential) => this.postLogin(data.user.uid))
    );
  }

  public sendPasswordResetEmail(email: string): Observable<any> {
    return from(this.firebaseAuth.auth.sendPasswordResetEmail(email));
  }

  public createUserWithEmailAndPassword(
    name: string,
    lastName: string,
    phone: string,
    email: string,
    password: string
  ): Observable<any> {
    return from(
      this.firebaseAuth.auth.createUserWithEmailAndPassword(email, password)
    ).pipe(
      switchMap((data: firebase.auth.UserCredential) => {
        const user: User = new User(null, 0, name, lastName, email, phone);
        return of(undefined).pipe(
          combineLatest(
            from(
              this.firebaseAuth.auth.currentUser.updateProfile({
                displayName: name + " " + lastName,
                photoURL: null,
              })
            ),
            this.userService.addUserForFirstTime(
              data.user.uid,
              data.user.email,
              user
            ),
            from(this.firebaseAuth.auth.currentUser.sendEmailVerification())
          ),
          tap(() => this.postLogin(data.user.uid))
        );
      })
    );
  }

  postLogin(uid: string) {
    if (!!this._userServiceSub) {
      this._userServiceSub.unsubscribe();
    }
    this._userServiceSub = this.userService.fetchUserByUid(uid).subscribe(
      (user: User) => {
        this._user.next(user);
      },
      (error) => {
        console.log(error);
        this.logout();
      }
    );
  }

  logout(): Observable<any> {
    return from(this.firebaseAuth.auth.signOut()).pipe(
      tap(() => location.reload())
    );
  }

  public getUserObservable(): Observable<User> {
    return this._user.asObservable();
  }

  get user(): User {
    return this._user.value;
  }
}
