import { Injectable } from "@angular/core";
import { AngularFireStorage } from "@angular/fire/storage";
import { AngularFirestore } from "angularfire2/firestore";
import { Observable, from } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class FileService {
  constructor(
    private afs: AngularFirestore,
    private storage: AngularFireStorage
  ) {}

  uploadFile(
    event,
    uid,
    fileName
  ): { task: Observable<any>; percentage: Observable<number>; fileRef: any } {
    const file = event.target.files[0];
    const filePath = uid + "/" + fileName;
    const fileRef = this.storage.ref(filePath);
    //const task = ref.put(file);
    //firebase.storage.UploadMetadata
    const task = this.storage.upload(filePath, file);

    const uploadPercent = task.percentageChanges();

    return {
      task: task.snapshotChanges(),
      percentage: uploadPercent,
      fileRef: fileRef,
    };
  }
}
