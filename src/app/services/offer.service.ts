import { Injectable } from "@angular/core";
import { AngularFirestore, DocumentReference } from "angularfire2/firestore";
import { Offer, OfferInterface } from "../models/offer";
import { Observable, from } from "rxjs";
import { map } from "rxjs/operators";
import {
  OfferStatus,
  publicOfferingsKey,
  offersKey,
} from "src/utils/constants";

@Injectable({
  providedIn: "root",
})
export class OfferService {
  constructor(private afs: AngularFirestore) {}

  addOffer(uid: string, offer: Offer): Observable<Offer> {
    const dat: OfferInterface = {
      ...offer.toJSON(),
      status: OfferStatus.recentlyAdded,
      bidderUID: uid,
    };
    return from(
      this.afs.collection(offersKey).doc(uid).collection(offersKey).add(dat)
    ).pipe(
      map((data: DocumentReference) => {
        return Offer.fromJSON({ ...dat, key: data.id });
      })
    );
  }

  updateOffer(
    uid: string,
    key: string,
    oldStatus: OfferStatus,
    newStatus: OfferStatus,
    offerObj: OfferInterface
  ): Observable<OfferStatus> {
    var batch = this.afs.firestore.batch();

    if (
      (oldStatus === OfferStatus.recentlyAdded &&
        newStatus === OfferStatus.onlyForUser) ||
      (oldStatus === OfferStatus.onlyForUser &&
        newStatus === OfferStatus.onlyForUser)
    ) {
      const offer: OfferInterface = {
        ...offerObj,
        status: newStatus,
        publicOffer: false,
      };

      batch.update(
        this.afs.collection(offersKey).doc(uid).collection(offersKey).doc(key)
          .ref,
        offer
      );
    } else if (
      (oldStatus === OfferStatus.recentlyAdded &&
        newStatus === OfferStatus.publicOffer) ||
      (oldStatus === OfferStatus.onlyForUser &&
        newStatus === OfferStatus.publicOffer)
    ) {
      const offer: OfferInterface = {
        ...offerObj,
        status: newStatus,
        publicOffer: true,
      };
      batch.set(this.afs.collection(publicOfferingsKey).doc(key).ref, offer);
      batch.update(
        this.afs.collection(offersKey).doc(uid).collection(offersKey).doc(key)
          .ref,
        offer
      );
    } else if (
      oldStatus === OfferStatus.publicOffer &&
      newStatus === OfferStatus.publicOffer
    ) {
      const offer: OfferInterface = {
        ...offerObj,
        status: newStatus,
        publicOffer: true,
      };
      batch.update(this.afs.collection(publicOfferingsKey).doc(key).ref, offer);
      batch.update(
        this.afs.collection(offersKey).doc(uid).collection(offersKey).doc(key)
          .ref,
        offer
      );
    } else if (
      oldStatus === OfferStatus.publicOffer &&
      newStatus === OfferStatus.onlyForUser
    ) {
      const offer: OfferInterface = {
        ...offerObj,
        status: newStatus,
        publicOffer: false,
      };
      batch.delete(this.afs.collection(publicOfferingsKey).doc(key).ref);
      batch.update(
        this.afs.collection(offersKey).doc(uid).collection(offersKey).doc(key)
          .ref,
        offer
      );
    }
    return from(batch.commit()).pipe(map(() => newStatus));
  }

  fetchOffer(uid: string, key: string): Observable<Offer> {
    return this.afs
      .collection(offersKey)
      .doc(uid)
      .collection(offersKey)
      .doc(key)
      .snapshotChanges()
      .pipe(
        map((a) => {
          const data = a.payload.data() as any;
          const id = a.payload.id;
          const dat: OfferInterface = { key: id, ...data };
          return Offer.fromJSON(dat);
        })
      );
  }

  fetchPublicOffers(): Observable<Offer[]> {
    return this.afs
      .collection(publicOfferingsKey)
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data: any = a.payload.doc.data();
            const offer: OfferInterface = {
              key: a.payload.doc.id,
              ...data,
            };
            return Offer.fromJSON(offer);
          })
        )
      );
  }

  fetchMyOffers(uid: string): Observable<Offer[]> {
    return this.afs
      .collection(offersKey)
      .doc(uid)
      .collection(offersKey)
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const offer: OfferInterface = {
              key: a.payload.doc.id,
              ...a.payload.doc.data(),
            };
            return Offer.fromJSON(offer);
          })
        )
      );
  }
}
