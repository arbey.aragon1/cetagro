import { Injectable } from "@angular/core";
import { AngularFirestore } from "angularfire2/firestore";
import { User } from "../models/user";
import { Observable, from } from "rxjs";
import { map, tap, take } from "rxjs/operators";
import { userKey, emailKey } from "src/utils/constants";

export interface Item {
  id: string;
  name: string;
}

export interface Email {
  exist: boolean;
}

@Injectable({
  providedIn: "root",
})
export class UserService {
  constructor(private afs: AngularFirestore) {}

  addUserForFirstTime(uid: string, email: string, user: User): Observable<any> {
    var batch = this.afs.firestore.batch();
    batch.set(this.afs.collection(userKey).doc(uid).ref, user.toJSON());
    batch.set(this.afs.collection(emailKey).doc(email).ref, {
      exist: true,
    } as Email);
    return from(batch.commit()).pipe(take(1));
  }

  fetchEmail(email: string): Observable<boolean> {
    return from(this.afs.collection(emailKey).doc(email).valueChanges()).pipe(
      map((val) => {
        console.log(val);
        return !!val;
      }),
      take(1)
    );
  }

  fetchUserByEmail(email: string): Observable<User> {
    return from(
      this.afs
        .collection(userKey, (ref) => ref.where("email", "==", email))
        .valueChanges()
        .pipe(
          map((l: any[]) => {
            const a = l[0];
            const data = a.payload.data() as any;
            const id = a.payload.id;
            return User.fromJSON({ uid: id, ...data });
          })
        )
    );
  }

  fetchUserByUid(uid: string): Observable<User> {
    return from(
      this.afs
        .collection(userKey)
        .doc(uid)
        .snapshotChanges()
        .pipe(
          map((a) => {
            const data = a.payload.data() as any;
            const id = a.payload.id;
            return User.fromJSON({ uid: id, ...data });
          })
        )
    );
  }
}
