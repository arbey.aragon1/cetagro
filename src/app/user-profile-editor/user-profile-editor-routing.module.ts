import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserProfileEditorPage } from './user-profile-editor.page';

const routes: Routes = [
  {
    path: '',
    component: UserProfileEditorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserProfileEditorPageRoutingModule {}
