import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { UserProfileEditorPageRoutingModule } from "./user-profile-editor-routing.module";

import { UserProfileEditorPage } from "./user-profile-editor.page";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserProfileEditorPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [UserProfileEditorPage],
})
export class UserProfileEditorPageModule {}
