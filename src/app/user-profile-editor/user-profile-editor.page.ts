import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
  AbstractControl,
} from "@angular/forms";
import { NavController } from "@ionic/angular";
import { AuthenticateService } from "../services/authenticate.service";

@Component({
  selector: "app-user-profile-editor",
  templateUrl: "./user-profile-editor.page.html",
  styleUrls: ["./user-profile-editor.page.scss"],
})
export class UserProfileEditorPage implements OnInit {
  registerForm: FormGroup;

  validation_message = {
    name: [{ type: "required", message: "El email es requerido" }],
    lastName: [{ type: "required", message: "El apellido es requerido" }],
    companyName: [
      { type: "required", message: "El nombre de la empresa es requerido" },
    ],
    phone: [{ type: "required", message: "El apellido es requerido" }],
    email: [
      { type: "required", message: "El email es requerido" },
      { type: "pattern", message: "Este no es un email valido" },
    ],
    password: [
      { type: "required", message: "El password es requerido" },
      { type: "minlength", message: "Minimo 6 letras para el password" },
    ],
  };

  errorMessage = "";

  constructor(
    private formBuilder: FormBuilder,
    private navCtrl: NavController,
    private auth: AuthenticateService
  ) {}

  ngOnInit() {
    this.registerForm = this.formBuilder.group(
      {
        name: new FormControl("", Validators.compose([Validators.required])),
        lastName: new FormControl(
          "",
          Validators.compose([Validators.required])
        ),
        companyName: new FormControl(
          "",
          Validators.compose([Validators.required])
        ),
        phone: new FormControl("", Validators.compose([Validators.required])),
        email: new FormControl(
          "",
          Validators.compose([
            Validators.required,
            Validators.pattern(
              "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"
            ),
          ])
        ),
        password: new FormControl(
          "",
          Validators.compose([Validators.required, Validators.minLength(6)])
        ),
        confirmPassword: new FormControl(
          "",
          Validators.compose([Validators.required])
        ),
      },
      { validator: this.passwordConfirming }
    );
  }

  passwordConfirming(c: AbstractControl): { invalid: boolean } {
    if (c.get("password").value !== c.get("confirmPassword").value) {
      return { invalid: true };
    }
  }

  registerUser(credentials) {
    console.log(credentials);
    this.auth
      .createUserWithEmailAndPassword(
        credentials.name,
        credentials.lastName,
        credentials.phone,
        credentials.email,
        credentials.password
      )
      .subscribe(() => {
        this.navCtrl.navigateForward("/menu/home");
      });
  }

  goToBack() {
    this.navCtrl.navigateBack("/login");
  }
}
