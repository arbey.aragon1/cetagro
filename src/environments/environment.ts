// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB9kvtLECzJvopIhgUq61BO6cfuLslct50",
    authDomain: "cetagro-977e6.firebaseapp.com",
    databaseURL: "https://cetagro-977e6.firebaseio.com",
    projectId: "cetagro-977e6",
    storageBucket: "cetagro-977e6.appspot.com",
    messagingSenderId: "981786208336",
    appId: "1:981786208336:web:7500ff4abb5364f7ad4955",
    measurementId: "G-HZ8JTY6KMB"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
