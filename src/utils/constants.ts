export const EMAIL_REGEX: string =
  "^[a-zA-Z0-9]+(.[_a-zA-Z0-9]+)*@[a-zA-Z0-9-]+(.[a-zA-Z0-9-]+)*(.[a-zA-Z]{2,15})$";
export const PHONE_REGEX: string = "[0-9]+";

export const enum OfferStatus {
  recentlyAdded,
  onlyForUser,
  publicOffer,
}
//updatedByUser

export const userKey: string = "users";
export const emailKey: string = "emails";
export const publicOfferingsKey: string = "public-offerings";
export const offersKey: string = "offers";
